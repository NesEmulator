
#include "nes6502.h"
#include "nes6502i.h"
#include "types.h"


static nes6502_context cpu;
static uint8 *ram = NULL, *stack = NULL;
static uint8 null_page[NES6502_BANKSIZE];

#define EXPAND_FLAGS() \
  c_flag = (cpu.p_reg) & NES6502_C_FLAG; \
  z_flag = (cpu.p_reg) & NES6502_Z_FLAG; \
  i_flag = (cpu.p_reg) & NES6502_I_FLAG; \
  d_flag = (cpu.p_reg) & NES6502_D_FLAG; \
  b_flag = (cpu.p_reg) & NES6502_B_FLAG; \
  v_flag = (cpu.p_reg) & NES6502_V_FLAG; \
  n_flag = (cpu.p_reg) & NES6502_N_FLAG;

#define COMPACT_FLAGS() \
  cpu.p_reg = NES6502_R_FLAG;              \
  if (c_flag) cpu.p_reg |= NES6502_C_FLAG; \
  if (z_flag) cpu.p_reg |= NES6502_Z_FLAG; \
  if (i_flag) cpu.p_reg |= NES6502_I_FLAG; \
  if (d_flag) cpu.p_reg |= NES6502_D_FLAG; \
  if (b_flag) cpu.p_reg |= NES6502_B_FLAG; \
  if (v_flag) cpu.p_reg |= NES6502_V_FLAG; \
  if (n_flag) cpu.p_reg |= NES6502_N_FLAG;


#define IF_BREAK()     b_flag
#define IF_CARRY()     c_flag
#define IF_DECIMAL()   d_flag
#define IF_INTERRUPT() i_flag
#define IF_OVERFLOW()  v_flag
#define IF_NEGATIVE()  n_flag
#define IF_ZERO()      z_flag

#define SET_BREAK(v)     b_flag = (v);
#define SET_CARRY(v)     c_flag = (v);
#define SET_DECIMAL(v)   d_flag = (v);
#define SET_INTERRUPT(v) i_flag = (v);
#define SET_OVERFLOW(v)  v_flag = (v);
#define SET_SIGN(v)      n_flag = (v);
#define SET_ZERO(v)      z_flag = (v);

#define BRANCH_ON(cond) \
  if (cond) {                                       \
    cpu.total_cycles++;                             \
    cpu.remaining_cycles--;                         \
    if ((oper & 0xFF00) != (cpu.pc_reg & 0xFF00)) { \
      cpu.total_cycles++;                           \
      cpu.remaining_cycles--;                       \
    }                                               \
    cpu.pc_reg = oper;                              \
  }

#define CHECK_ZERO(v)   SET_ZERO ((v) == 0)
#define CHECK_SIGN(v)   SET_SIGN ((v) & 0x80)

#define CHECK_ZERO_AND_SIGN(v)  CHECK_ZERO (v) CHECK_SIGN (v)

#define WB() \
  if (opcode->addressing == ADDR_ACCUMULATOR) \
    cpu.a_reg = oper;                         \
  else                                        \
    WB_SIMPLE ()

#define WB_SIMPLE() nes6502_writebyte (operp, oper);

#define PULL(v) \
  cpu.s_reg = (cpu.s_reg + 1) & 0xFF; \
  (v) = stack[cpu.s_reg];

#define PULL_W(v) \
  cpu.s_reg = (cpu.s_reg + 1) & 0xFF;     \
  (v) = stack[cpu.s_reg];                 \
  cpu.s_reg = (cpu.s_reg + 1) & 0xFF;     \
  (v) |= ((uint32) stack[cpu.s_reg]) << 8;

#define PUSH(v) \
  stack[cpu.s_reg] = v;               \
  cpu.s_reg = (cpu.s_reg - 1) & 0xFF;

#define PUSH_W(v) \
  stack[cpu.s_reg] = (v) >> 8;        \
  cpu.s_reg = (cpu.s_reg - 1) & 0xFF; \
  stack[cpu.s_reg] = (v) & 0xFF;      \
  cpu.s_reg = (cpu.s_reg - 1) & 0xFF;

#define BANK(address) \
  cpu.mem_page[(address) >> NES6502_BANKSHIFT][(address) & NES6502_BANKMASK]

#define BANK_W(address) \
  (((uint32) BANK (address)) | (((uint32) BANK ((address) + 1)) << 8))

#define IRQ_PROC(vector) \
  cpu.burn_cycles += NES6502_INT_CYCLES;           \
  PUSH_W (cpu.pc_reg)                              \
  cpu.p_reg &= ~(NES6502_B_FLAG);                  \
  PUSH (cpu.p_reg)                                 \
  cpu.p_reg |= NES6502_I_FLAG;                     \
  cpu.pc_reg = BANK_W (vector);


void nes6502_power (void)
{
  cpu.a_reg = 0;
  cpu.x_reg = cpu.y_reg = 0;
  cpu.s_reg = 0xFF;
  cpu.total_cycles = 0;
  nes6502_reset ();
}

void nes6502_reset (void)
{
  cpu.pc_reg = BANK (NES6502_RESET_VECTOR);
  cpu.pc_reg |= ((uint32) BANK (NES6502_RESET_VECTOR + 1)) << 8;
  cpu.p_reg = NES6502_Z_FLAG | NES6502_R_FLAG;
  cpu.burn_cycles = NES6502_RESET_CYCLES;
  cpu.int_pending = FALSE;
  cpu.halted = FALSE;
  cpu.remaining_cycles = 0;
}

void nes6502_halt (void)
{
  cpu.halted = TRUE;
  cpu.remaining_cycles = 0;
}

int nes6502_ishalted (void)
{
  return cpu.halted;
}


void nes6502_nmi (void)
{
  if (cpu.halted) return;
  cpu.remaining_cycles = 0;
  IRQ_PROC (NES6502_NMI_VECTOR)
}

void nes6502_irq (void)
{
  if (cpu.halted) return;
  cpu.remaining_cycles = 0;
  if (cpu.p_reg & NES6502_I_FLAG) {
    cpu.int_pending = TRUE;
  } else {
    IRQ_PROC (NES6502_IRQ_VECTOR)
  }
}

#ifdef DEBUGGER_ENABLED
static
int nes6502_checkaddr (uint32 addr, int32 flags)
{
  uint32 i;
  for (i = 0; i < cpu.num_bp; i++) {
    if (cpu.bp_flags[i] & NES6502_DEBUG_DISABLED)
      continue;
    if (!(cpu.bp_flags[i] & flags))
      continue;
    if ((cpu.bp_mask[i] & addr) == cpu.bp_addr[i])
      return 1;
  }
  return 0;
}

static
int nes6502_checkstack (uint32 count, int push)
{
  uint32 operp = cpu.s_reg;
  if (push) {
    while (count--) {
      if (nes6502_checkaddr (NES6502_STACK_OFFSET + operp, NES6502_DEBUG_WRITE))
        return 1;
      operp = (operp - 1) & 0xFF;
    }
  } else {
    while (count--) {
      operp = (operp + 1) & 0xFF;
      if (nes6502_checkaddr (NES6502_STACK_OFFSET + operp, NES6502_DEBUG_READ))
        return 1;
    }
  }
  return 0;
}

static
void nes6502_debug (int first_insn)
{
  uint8 opc;
  uint32 operp, t1, t2;
  int32 flags = 0;
  struct nes6502_opcode *opcode;
  enum nes6502_instruction insn;
  int details;

  if (nes6502_checkaddr (cpu.pc_reg, NES6502_DEBUG_READ))
    goto bphit;

  opc = BANK (cpu.pc_reg);
  opcode = &nes6502_opcodes[opc];

  for (t1 = 1; t1 < nes6502_addressing_length[opcode->addressing]; t1++) {
    if (nes6502_checkaddr (cpu.pc_reg + t1, NES6502_DEBUG_READ))
      goto bphit;
  }

  insn = opcode->instruction;
  details = nes6502_instructions[insn].details;

  if (details & I_RD) {
    flags |= NES6502_DEBUG_READ;
  }
  if (details & I_WR) {
    flags |= NES6502_DEBUG_WRITE;
  }

  switch (opcode->addressing) {
  case ADDR_ZERO_PAGE:
    operp = (uint32) BANK (cpu.pc_reg + 1);
    if (nes6502_checkaddr (operp, flags)) goto bphit;
    break;
  case ADDR_ZERO_PAGE_X:
    operp = (BANK (cpu.pc_reg + 1) + cpu.x_reg) & 0xFF;
    if (nes6502_checkaddr (operp, flags)) goto bphit;
    break;
  case ADDR_ZERO_PAGE_Y:
    operp = (BANK (cpu.pc_reg + 1) + cpu.y_reg) & 0xFF;
    if (nes6502_checkaddr (operp, flags)) goto bphit;
    break;
  case ADDR_ABSOLUTE:
    operp = BANK_W (cpu.pc_reg + 1);
    if (nes6502_checkaddr (operp, flags)) goto bphit;
    break;
  case ADDR_ABSOLUTE_X:
    operp = (BANK_W (cpu.pc_reg + 1) + cpu.x_reg) & 0xFFFF;
    if (nes6502_checkaddr (operp, flags)) goto bphit;
    break;
  case ADDR_ABSOLUTE_Y:
    operp = (BANK_W (cpu.pc_reg + 1) + cpu.y_reg) & 0xFFFF;
    if (nes6502_checkaddr (operp, flags)) goto bphit;
    break;
  case ADDR_INDIRECT:
    operp = BANK_W (cpu.pc_reg + 1);
    if (nes6502_checkaddr (operp, NES6502_DEBUG_READ)) goto bphit;
    if ((operp & 0xFF) == 0xFF) {
      if (nes6502_checkaddr (operp - 0xFF, NES6502_DEBUG_READ)) goto bphit;
    } else {
      if (nes6502_checkaddr (operp + 1, NES6502_DEBUG_READ)) goto bphit;
    }
    break;
  case ADDR_INDIRECT_X:
    t1 = (BANK (cpu.pc_reg + 1) + cpu.x_reg) & 0xFF;
    if (nes6502_checkaddr (t1, NES6502_DEBUG_READ)) goto bphit;
    if (nes6502_checkaddr ((t1 + 1) & 0xFF, NES6502_DEBUG_READ)) goto bphit;
    operp = (uint32) ram[t1];
    operp |= ((uint32) ram[(t1 + 1) & 0xFF]) << 8;
    if (nes6502_checkaddr (operp, flags)) goto bphit;
    break;
  case ADDR_INDIRECT_Y:
    t1 = (BANK (cpu.pc_reg + 1)) & 0xFF;
    if (nes6502_checkaddr (t1, NES6502_DEBUG_READ)) goto bphit;
    if (nes6502_checkaddr ((t1 + 1) & 0xFF, NES6502_DEBUG_READ)) goto bphit;
    operp = (uint32) ram[t1] + cpu.y_reg;
    operp += ((uint32) ram[(t1 + 1) & 0xFF]) << 8;
    operp &= 0xFFFF;
    if (nes6502_checkaddr (operp, flags)) goto bphit;
    break;
  default:
    break;
  }

  if (details & I_ST) {
    if (details & I_JP) {
      if (insn == INSN_BRK || insn == INSN_JSR) {
        t1 = 1;
        if (insn == INSN_BRK) t2 = 3;
        else t2 = 2;
      } else {
        t1 = 0;
        if (insn == INSN_RTI) t2 = 3;
        else t2 = 2;
      }
    } else {
      t2 = 1;
      if (insn == INSN_PHA || insn == INSN_PHP) {
        t1 = 1;
      } else {
        t1 = 0;
      }
    }
    if (nes6502_checkstack (t2, t1))
      goto bphit;
  }

  return;

bphit:
  (*cpu.debug_handler) (first_insn);

  return;
}

int nes6502_addbreakpoint (uint32 address, uint32 mask, int32 flags)
{
  if (cpu.num_bp == NES6502_MAX_BREAKPOINTS) return -1;
  nes6502_editbreakpoint (cpu.num_bp, address, mask, flags);
  return cpu.num_bp++;
}

void nes6502_editbreakpoint (uint32 bp, uint32 address, uint32 mask, int32 flags)
{
  if (bp >= NES6502_MAX_BREAKPOINTS) return;
  cpu.bp_addr[bp] = address;
  cpu.bp_mask[bp] = mask;
  cpu.bp_flags[bp] = flags;
}

void nes6502_getbreakpoint (uint32 bp, uint32 *address, uint32 *mask, int32 *flags)
{
  if (bp >= NES6502_MAX_BREAKPOINTS) return;
  *address = cpu.bp_addr[bp];
  *mask = cpu.bp_mask[bp];
  *flags = cpu.bp_flags[bp];
}

void nes6502_removebreakpoint (uint32 bp)
{
  uint32 i;
  if (bp >= cpu.num_bp) return;
  cpu.num_bp--;
  for (i = bp; i < cpu.num_bp; i++) {
    cpu.bp_addr[i] = cpu.bp_addr[i + 1];
    cpu.bp_mask[i] = cpu.bp_mask[i + 1];
    cpu.bp_flags[i] = cpu.bp_flags[i + 1];
  }
}

uint32 nes6502_numbreakpoints (void)
{
  return cpu.num_bp;
}

#endif


int32 nes6502_execute (int32 cycles)
{
  int old_cycles;
  int c_flag, z_flag, i_flag, d_flag;
  int b_flag, v_flag, n_flag;

#ifdef DEBUGGER_ENABLED
  int first_insn = TRUE;
#endif

  if (cpu.halted) return 0;
  old_cycles = cpu.total_cycles;

  if (cycles <= 0)
    cycles = cpu.burn_cycles + 1;
  cpu.remaining_cycles = cycles;

  EXPAND_FLAGS ()

  if (!IF_INTERRUPT () && cpu.int_pending) {
    cpu.int_pending = FALSE;
    IRQ_PROC (NES6502_IRQ_VECTOR);
  }

  if (cpu.burn_cycles && cpu.remaining_cycles > 0) {
    int burn_for;

    burn_for = MIN (cpu.remaining_cycles, cpu.burn_cycles);
    cpu.total_cycles += burn_for;
    cpu.burn_cycles -= burn_for;
    cpu.remaining_cycles -= burn_for;
  }

  while (cpu.remaining_cycles > 0) {
    uint8 opc;
    struct nes6502_opcode *opcode;
    uint32 oper = 0, operp = 0, t;
    enum nes6502_instruction insn;
    int details;

    opc = BANK (cpu.pc_reg);
    opcode = &nes6502_opcodes[opc];
    insn = opcode->instruction;
    details = nes6502_instructions[insn].details;

#ifdef DEBUGGER_ENABLED
    if (cpu.num_bp > 0) {
      nes6502_debug (first_insn);
      if (cpu.remaining_cycles <= 0) break;
    }
    first_insn = FALSE;
#endif

    cpu.total_cycles += opcode->cycles;
    cpu.remaining_cycles -= opcode->cycles;
    cpu.pc_reg++;

    switch (opcode->addressing) {
      case ADDR_ACCUMULATOR:
        oper = cpu.a_reg;
        break;
      case ADDR_IMMEDIATE:
        oper = (uint32) BANK (cpu.pc_reg);
        cpu.pc_reg++;
        break;
      case ADDR_ZERO_PAGE:
        operp = (uint32) BANK (cpu.pc_reg);
        oper = (uint32) ram[operp];
        cpu.pc_reg++;
        break;
      case ADDR_ZERO_PAGE_X:
        operp = (BANK (cpu.pc_reg) + cpu.x_reg) & 0xFF;
        oper = ram[operp];
        cpu.pc_reg++;
        break;
      case ADDR_ZERO_PAGE_Y:
        operp = (BANK (cpu.pc_reg) + cpu.y_reg) & 0xFF;
        oper = ram[operp];
        cpu.pc_reg++;
        break;
      case ADDR_ABSOLUTE:
        operp = BANK_W (cpu.pc_reg);
        cpu.pc_reg += 2;
        if (details & I_RD)
          oper = nes6502_readbyte (operp);
        break;
      case ADDR_ABSOLUTE_X:
        operp = (BANK (cpu.pc_reg) + cpu.x_reg);
        cpu.pc_reg++;
        if (operp & 0x100) cpu.total_cycles++;
        operp += ((uint32) BANK (cpu.pc_reg)) << 8;
        cpu.pc_reg++;
        operp &= 0xFFFF;
        if (details & I_RD)
          oper = nes6502_readbyte (operp);
        break;
      case ADDR_ABSOLUTE_Y:
        operp = (BANK (cpu.pc_reg) + cpu.y_reg);
        cpu.pc_reg++;
        if (operp & 0x100) cpu.total_cycles++;
        operp += ((uint32) BANK (cpu.pc_reg)) << 8;
        cpu.pc_reg++;
        operp &= 0xFFFF;
        if (details & I_RD)
          oper = nes6502_readbyte (operp);
        break;
      case ADDR_INDIRECT:
        t = BANK_W (cpu.pc_reg);
        cpu.pc_reg += 2;
        /* 6502 bug here! */
        operp = BANK (t);
        if ((t & 0xFF) == 0xFF)
          operp |= ((uint32) BANK (t - 0xFF)) << 8;
        else
          operp |= ((uint32) BANK (t + 1)) << 8;
        /* We don't need to eval "oper" because ADDR_INDIRECT
         * is used only for the JMP instruction
         * */
        break;
      case ADDR_INDIRECT_X:
        t = (BANK (cpu.pc_reg) + cpu.x_reg) & 0xFF;
        cpu.pc_reg++;
        operp = (uint32) ram[t];
        operp |= ((uint32) ram[(t + 1) & 0xFF]) << 8;
        if (details & I_RD)
          oper = nes6502_readbyte (operp);
        break;
      case ADDR_INDIRECT_Y:
        t = BANK (cpu.pc_reg) & 0xFF;
        cpu.pc_reg++;
        operp = ((uint32) ram [t]) + cpu.y_reg;
        if (operp & 0x100) cpu.total_cycles++;
        operp += ((uint32) ram [(t + 1) & 0xFF]) << 8;
        operp &= 0xFFFF;
        if (details & I_RD)
          oper = nes6502_readbyte (operp);
        break;
      case ADDR_RELATIVE:
        t = BANK (cpu.pc_reg);
        cpu.pc_reg++;
        if (t & 0x80) t |= 0xFF00;
        oper = (cpu.pc_reg + t) & 0xFFFF;
        break;
      case ADDR_IMPLIED:
      default:
        oper = 0;
        break;
    }


    switch (insn) {
      case INSN_AAC: /* Undocumented */
        cpu.a_reg &= oper;
        CHECK_ZERO_AND_SIGN (cpu.a_reg)
        SET_CARRY (IF_ZERO ())
        break;
      case INSN_AAX: /* Undocumented */
        oper = cpu.x_reg & cpu.a_reg;
        WB_SIMPLE ()
        break;
      case INSN_ADC:
        t = oper + cpu.a_reg;
        if (IF_CARRY ()) t++;
        SET_CARRY (t & 0x100)
        SET_OVERFLOW (!((cpu.a_reg ^ oper) & 0x80) && ((cpu.a_reg ^ t) & 0x80))
        cpu.a_reg = t & 0xFF;
        CHECK_ZERO_AND_SIGN (cpu.a_reg)
        break;
      case INSN_AND:
        cpu.a_reg &= oper;
        CHECK_ZERO_AND_SIGN (cpu.a_reg)
        break;
      case INSN_ARR: /* Undocumented */
        cpu.a_reg &= oper;
        cpu.a_reg >>= 1;
        if (IF_CARRY ()) cpu.a_reg |= 0x80;
        SET_CARRY (cpu.a_reg & 0x40)
        SET_OVERFLOW (((cpu.a_reg >> 1) ^ cpu.a_reg) & 0x20)
        CHECK_ZERO_AND_SIGN (cpu.a_reg)
        break;
      case INSN_ASL:
        SET_CARRY (oper & 0x80)
        oper = (oper << 1) & 0xFF;
        CHECK_ZERO_AND_SIGN (oper)
        WB ()
        break;
      case INSN_ASR: /* Undocumented */
        cpu.a_reg &= oper;
        SET_CARRY (cpu.a_reg & 0x01)
        cpu.a_reg >>= 1;
        SET_ZERO (cpu.a_reg == 0)
        SET_SIGN (0)
        break;
      case INSN_ATX: /* Undocumented */
        cpu.a_reg = cpu.x_reg = ((cpu.a_reg | 0xEE) & oper); /* Unpredictable */
        CHECK_ZERO_AND_SIGN (oper)
        break;
      case INSN_AXA: /* Undocumented */
        oper = cpu.a_reg & cpu.x_reg & ((uint8) ((operp >> 8) + 1)); /* Unpredictable */
        WB_SIMPLE ()
        break;
      case INSN_AXS: /* Undocumented */
        cpu.x_reg = ((cpu.a_reg & cpu.x_reg) - oper);
        SET_CARRY (cpu.x_reg < 0x100)
        cpu.x_reg &= 0xFF;
        CHECK_ZERO_AND_SIGN (cpu.x_reg)
        break;
      case INSN_BCC:
        BRANCH_ON (!IF_CARRY ())
        break;
      case INSN_BCS:
        BRANCH_ON (IF_CARRY ())
        break;
      case INSN_BEQ:
        BRANCH_ON (IF_ZERO ())
        break;
      case INSN_BIT:
        SET_SIGN (oper & 0x80)
        SET_OVERFLOW (oper & 0x40)
        SET_ZERO ((oper & cpu.a_reg) == 0)
        break;
      case INSN_BMI:
        BRANCH_ON (IF_NEGATIVE ())
        break;
      case INSN_BNE:
        BRANCH_ON (!IF_ZERO ())
        break;
      case INSN_BPL:
        BRANCH_ON (!IF_NEGATIVE ())
        break;
      case INSN_BRK:
        /* RTI from break causes the next instruction to be ignored */
        cpu.pc_reg++;
        PUSH_W (cpu.pc_reg)
        SET_BREAK (1)
        COMPACT_FLAGS ()
        PUSH (cpu.p_reg)
        SET_INTERRUPT (1)
        cpu.pc_reg = BANK_W (NES6502_IRQ_VECTOR);
        break;
      case INSN_BVC:
        BRANCH_ON (!IF_OVERFLOW ())
        break;
      case INSN_BVS:
        BRANCH_ON (IF_OVERFLOW ())
        break;
      case INSN_CLC:
        SET_CARRY (0)
        break;
      case INSN_CLD:
        SET_DECIMAL (0)
        break;
      case INSN_CLI:
        SET_INTERRUPT (0)
        if (cpu.int_pending && cpu.remaining_cycles > 0) {
          cpu.int_pending = FALSE;
          COMPACT_FLAGS ()
          IRQ_PROC (NES6502_IRQ_VECTOR)
        }
        break;
      case INSN_CLV:
        SET_OVERFLOW (0)
        break;
      case INSN_CMP:
        oper = cpu.a_reg - oper;
        SET_CARRY (oper < 0x100)
        oper &= 0xff;
        CHECK_ZERO_AND_SIGN (oper)
        break;
      case INSN_CPX:
        oper = cpu.x_reg - oper;
        SET_CARRY (oper < 0x100)
        oper &= 0xff;
        CHECK_ZERO_AND_SIGN (oper)
        break;
      case INSN_CPY:
        oper = cpu.y_reg - oper;
        SET_CARRY (oper < 0x100)
        oper &= 0xff;
        CHECK_ZERO_AND_SIGN (oper)
        break;
      case INSN_DCP: /* Undocumented */
        oper = (oper - 1) & 0xFF;
        WB_SIMPLE ()
        oper = cpu.a_reg - oper;
        SET_CARRY (oper < 0x100)
        oper &= 0xff;
        CHECK_ZERO_AND_SIGN (oper)
        break;
      case INSN_DEC:
        oper = (oper - 1) & 0xFF;
        CHECK_ZERO_AND_SIGN (oper)
        WB_SIMPLE ()
        break;
      case INSN_DEX:
        cpu.x_reg = (cpu.x_reg - 1) & 0xFF;
        CHECK_ZERO_AND_SIGN (cpu.x_reg)
        break;
      case INSN_DEY:
        cpu.y_reg = (cpu.y_reg - 1) & 0xFF;
        CHECK_ZERO_AND_SIGN (cpu.y_reg)
        break;
      case INSN_DOP: /* Undocumented */
        /* Do nothing */
        break;
      case INSN_EOR:
        cpu.a_reg ^= oper;
        CHECK_ZERO_AND_SIGN (cpu.a_reg)
        break;
      case INSN_HLT:
        cpu.remaining_cycles = 0;
        cpu.halted = TRUE;
        break;
      case INSN_INC:
        oper = (oper + 1) & 0xFF;
        CHECK_ZERO_AND_SIGN (oper)
        WB_SIMPLE ()
        break;
      case INSN_INX:
        cpu.x_reg = (cpu.x_reg + 1) & 0xFF;
        CHECK_ZERO_AND_SIGN (cpu.x_reg)
        break;
      case INSN_INY:
        cpu.y_reg = (cpu.y_reg + 1) & 0xFF;
        CHECK_ZERO_AND_SIGN (cpu.y_reg)
        break;
      case INSN_ISC: /* Undocumented */
        oper = (oper + 1) & 0xFF;
        WB_SIMPLE ()
        t = cpu.a_reg - oper;
        if (!IF_CARRY ()) t--;
        SET_CARRY (t < 0x100)
        SET_OVERFLOW (((cpu.a_reg ^ oper) & 0x80) && ((cpu.a_reg ^ t) & 0x80))
        cpu.a_reg = t & 0xFF;
        CHECK_ZERO_AND_SIGN (cpu.a_reg)
        break;
      case INSN_JMP:
        cpu.pc_reg = operp;
        break;
      case INSN_JSR:
        cpu.pc_reg--;
        PUSH_W (cpu.pc_reg)
        cpu.pc_reg = operp;
        break;
      case INSN_LAR: /* Undocumented */
        cpu.a_reg = cpu.x_reg = cpu.s_reg = ((cpu.s_reg) & oper);
        CHECK_ZERO_AND_SIGN (cpu.a_reg)
        break;
      case INSN_LAX: /* Undocumented */
        cpu.a_reg = cpu.x_reg = oper;
        CHECK_ZERO_AND_SIGN (cpu.a_reg)
        break;
      case INSN_LDA:
        cpu.a_reg = oper;
        CHECK_ZERO_AND_SIGN (cpu.a_reg)
        break;
      case INSN_LDX:
        cpu.x_reg = oper;
        CHECK_ZERO_AND_SIGN (cpu.x_reg)
        break;
      case INSN_LDY:
        cpu.y_reg = oper;
        CHECK_ZERO_AND_SIGN (cpu.y_reg)
        break;
      case INSN_LSR:
        SET_CARRY (oper & 0x01)
        oper >>= 1;
        SET_ZERO (oper == 0)
        SET_SIGN (0)
        WB ()
        break;
      case INSN_NOP:
        break;
      case INSN_ORA:
        cpu.a_reg |= oper;
        CHECK_ZERO_AND_SIGN (cpu.a_reg)
        break;
      case INSN_PHA:
        PUSH (cpu.a_reg)
        break;
      case INSN_PHP:
        COMPACT_FLAGS ()
        PUSH (cpu.p_reg)
        break;
      case INSN_PLA:
        PULL (cpu.a_reg)
        CHECK_ZERO_AND_SIGN (cpu.a_reg)
        break;
      case INSN_PLP:
        PULL (cpu.p_reg)
        EXPAND_FLAGS ()
        break;
      case INSN_RLA: /* Undocumented */
        oper <<= 1;
        if (IF_CARRY ()) oper |= 0x01;
        SET_CARRY (oper & 0x100)
        oper = oper & 0xFF;
        WB_SIMPLE ()
        cpu.a_reg &= oper;
        CHECK_ZERO_AND_SIGN (cpu.a_reg)
        break;
      case INSN_ROL:
        oper <<= 1;
        if (IF_CARRY ()) oper |= 0x01;
        SET_CARRY (oper & 0x100)
        oper = oper & 0xFF;
        CHECK_ZERO_AND_SIGN (oper)
        WB ()
        break;
      case INSN_ROR:
        if (IF_CARRY ()) oper |= 0x100;
        SET_CARRY (oper & 0x01)
        oper >>= 1;
        CHECK_ZERO_AND_SIGN (oper)
        WB ()
        break;
      case INSN_RRA: /* Undocumented */
        if (IF_CARRY ()) oper |= 0x100;
        SET_CARRY (oper & 0x01)
        oper >>= 1;
        WB_SIMPLE ()
        t = oper + cpu.a_reg;
        if (IF_CARRY ()) t++;
        SET_CARRY (t & 0x100)
        SET_OVERFLOW (!((cpu.a_reg ^ oper) & 0x80) && ((cpu.a_reg ^ t) & 0x80))
        cpu.a_reg = t & 0xFF;
        CHECK_ZERO_AND_SIGN (cpu.a_reg)
        break;
      case INSN_RTI:
        PULL (cpu.p_reg)
        EXPAND_FLAGS ()
        PULL_W (cpu.pc_reg)
        if (!IF_INTERRUPT () && cpu.int_pending && cpu.remaining_cycles > 0) {
          cpu.int_pending = FALSE;
          IRQ_PROC (NES6502_IRQ_VECTOR)
        }
        break;
      case INSN_RTS:
        PULL_W (cpu.pc_reg)
        cpu.pc_reg++;
        break;
      case INSN_SBC:
        t = cpu.a_reg - oper;
        if (!IF_CARRY ()) t--;
        SET_CARRY (t < 0x100)
        SET_OVERFLOW (((cpu.a_reg ^ oper) & 0x80) && ((cpu.a_reg ^ t) & 0x80))
        cpu.a_reg = t & 0xFF;
        CHECK_ZERO_AND_SIGN (cpu.a_reg)
        break;
      case INSN_SEC:
        SET_CARRY (1)
        break;
      case INSN_SED:
        SET_DECIMAL (1)
        break;
      case INSN_SEI:
        SET_INTERRUPT (1)
        break;
      case INSN_SLO: /* Undocumented */
        SET_CARRY (oper & 0x80)
        oper = (oper << 1) & 0xFF;
        WB_SIMPLE ()
        cpu.a_reg |= oper;
        CHECK_ZERO_AND_SIGN (cpu.a_reg)
        break;
      case INSN_SRE: /* Undocumented */
        SET_CARRY (oper & 0x01)
        oper >>= 1;
        WB_SIMPLE ()
        cpu.a_reg ^= oper;
        CHECK_ZERO_AND_SIGN (cpu.a_reg)
        break;
      case INSN_STA:
        nes6502_writebyte (operp, cpu.a_reg);
        break;
      case INSN_STX:
        nes6502_writebyte (operp, cpu.x_reg);
        break;
      case INSN_STY:
        nes6502_writebyte (operp, cpu.y_reg);
        break;
      case INSN_SXA: /* Undocumented */
        oper = cpu.x_reg & ((operp >> 8) + 1); /* Unpredictable */
        WB_SIMPLE ()
        break;
      case INSN_SYA: /* Undocumented */
        oper = cpu.y_reg & ((operp >> 8) + 1); /* Unpredictable */
        WB_SIMPLE ()
        break;
      case INSN_TAX:
        cpu.x_reg = cpu.a_reg;
        CHECK_ZERO_AND_SIGN (cpu.x_reg)
        break;
      case INSN_TAY:
        cpu.y_reg = cpu.a_reg;
        CHECK_ZERO_AND_SIGN (cpu.y_reg)
        break;
      case INSN_TOP: /* Undocumented */
        /* Do nothing */
        break;
      case INSN_TSX:
        cpu.x_reg = cpu.s_reg;
        CHECK_ZERO_AND_SIGN (cpu.x_reg)
        break;
      case INSN_TXA:
        cpu.a_reg = cpu.x_reg;
        CHECK_ZERO_AND_SIGN (cpu.a_reg)
        break;
      case INSN_TXS:
        cpu.s_reg = cpu.x_reg;
        break;
      case INSN_TYA:
        cpu.a_reg = cpu.y_reg;
        CHECK_ZERO_AND_SIGN (cpu.a_reg)
        break;
      case INSN_XAA: /* Undocumented */
        cpu.a_reg = (cpu.a_reg | 0xEE) & cpu.x_reg & oper;
        CHECK_ZERO_AND_SIGN (cpu.a_reg)
        break;
      case INSN_XAS: /* Undocumented */
        cpu.s_reg = cpu.x_reg & cpu.a_reg;
        oper = cpu.s_reg & ((operp >> 8) + 1);
        WB_SIMPLE ()
        break;
    }

  }

  COMPACT_FLAGS ()
  return (cpu.total_cycles - old_cycles);
}

/* read a byte from 6502 memory */
uint8 nes6502_readbyte (uint32 address)
{
  if (address < 0x800) {
    return ram[address];
  } else if (address < 0x8000) {
    nes6502_memread *mr;
    for (mr = cpu.read_handler; mr->min_range != 0xFFFFFFFF; mr++) {
      if (address >= mr->min_range && address <= mr->max_range)
        return mr->read_func (address);
    }
  }

  return BANK (address);
}

/* write a byte of data to 6502 memory */
void nes6502_writebyte (uint32 address, uint8 value)
{
  if (address < 0x800) {
    ram[address] = value;
    return;
  } else {
    nes6502_memwrite *mw;
    for (mw = cpu.write_handler; mw->min_range != 0xFFFFFFFF; mw++) {
      if (address >= mw->min_range && address <= mw->max_range) {
        mw->write_func (address, value);
        return;
      }
    }
  }

  BANK (address) = value;
}

uint8 nes6502_getbyte (uint32 address)
{
  return BANK (address);
}

void nes6502_putbyte (uint32 address, uint8 value)
{
  BANK (address) = value;
}


void nes6502_burn (int32 cycles)
{
  cpu.burn_cycles += cycles;
}

void nes6502_release (void)
{
  cpu.remaining_cycles = 0;
}

void nes6502_getcontext (nes6502_context *state)
{
  int loop;
  *state = cpu;
  for (loop = 0; loop < NES6502_NUMBANKS; loop++) {
    if (null_page == state->mem_page[loop])
      state->mem_page[loop] = NULL;
  }
}

void nes6502_setcontext (nes6502_context *state)
{
  int loop;
  cpu = *state;
  /* set dead page for all pages not pointed at anything */
  for (loop = 0; loop < NES6502_NUMBANKS; loop++) {
    if (NULL == cpu.mem_page[loop])
      cpu.mem_page[loop] = null_page;
  }

  ram = cpu.mem_page[0];
  stack = &ram[NES6502_STACK_OFFSET];

#ifdef DEBUGGER_ENABLED
  cpu.num_bp = 0;
#endif
}

