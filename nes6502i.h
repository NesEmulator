#ifndef __NES6502I_H
#define __NES6502I_H

#include "types.h"

enum nes6502_instruction {
  INSN_AAC = 0, /* [Undocumented]Logical "And" byte with accumulator. If result is negative then carry is set. */
  INSN_AAX, /* [Undocumented]Logical "And" X register with accumulator and store result in memory. (No flags changed) */
  INSN_ADC, /* Add Memory to Accumulator with Carry */
  INSN_AND, /* Logical "And" Memory with Accumulator */
  INSN_ARR, /* [Undocumented]Logical "And" byte with accumulator, then rotate one bit right in accumulator and check bit 5 and 6. */
  INSN_ASL, /* Shift Left One Bit (Memory or Accumulator) */
  INSN_ASR, /* [Undocumented]Logical "And" byte with accumulator, then shift right one bit in accumulator. */
  INSN_ATX, /* [Undocumented]Logical "And" byte with accumulator, then transfer accumulator to X register. Or it could be (A = X = ((A | 0xEE) & data)).  Highly unpredictable.*/
  INSN_AXA, /* [Undocumented]Logical "And" X register with accumulator then AND result with (ADDR_HI + 1) and store in memory. Unpredictable. */
  INSN_AXS, /* [Undocumented]Logical "And" X register with accumulator and store result in X register, then subtract byte from X register (without borrow). (carry should work the same way as CMP) */
  INSN_BCC, /* Branch on Carry Clear */
  INSN_BCS, /* Branch on Carry Set */
  INSN_BEQ, /* Branch on Result Zero */
  INSN_BIT, /* Test Bits in Memory with Accumulator */
  INSN_BMI, /* Branch on Result Minus */
  INSN_BNE, /* Branch on Result not Zero */
  INSN_BPL, /* Branch on Result Plus */
  INSN_BRK, /* Force Break */
  INSN_BVC, /* Branch on Overflow Clear */
  INSN_BVS, /* Branch on Overflow Set */
  INSN_CLC, /* Clear Carry Flag */
  INSN_CLD, /* Clear Decimal Mode */
  INSN_CLI, /* Clear interrupt Disable Bit */
  INSN_CLV, /* Clear Overflow Flag */
  INSN_CMP, /* Compare Memory and Accumulator */
  INSN_CPX, /* Compare Memory and Index X */
  INSN_CPY, /* Compare Memory and Index Y */
  INSN_DCP, /* [Undocumented]Subtract 1 from memory (without borrow) and then compare with accumulator */
  INSN_DEC, /* Decrement Memory by One */
  INSN_DEX, /* Decrement Index X by One */
  INSN_DEY, /* Decrement Index Y by One */
  INSN_DOP, /* [Undocumented]Double NOP (fetches memory) */
  INSN_EOR, /* Logical "Exclusive-Or" Memory with Accumulator */
  INSN_HLT, /* [Undocumented] Halt (JAM) 6502. */
  INSN_INC, /* Increment Memory by One */
  INSN_INX, /* Increment Index X by One */
  INSN_INY, /* Increment Index Y by One */
  INSN_ISC, /* [Undocumented]Increase memory by one, then subtract memory from accumulator (with borrow) like SBC. */
  INSN_JMP, /* Jump to New Location */
  INSN_JSR, /* Jump to New Location Saving Return Address */
  INSN_LAR, /* [Undocumented]AND memory with stack pointer, transfer result to accumulator, X register and stack pointer. */
  INSN_LAX, /* [Undocumented]Load accumulator and X register with memory. */
  INSN_LDA, /* Load Accumulator with Memory */
  INSN_LDX, /* Load Index X with Memory */
  INSN_LDY, /* Load Index Y with Memory */
  INSN_LSR, /* Shift Right One Bit (Memory or Accumulator) */
  INSN_NOP, /* No Operation */
  INSN_ORA, /* Logical "Or" Memory with Accumulator */
  INSN_PHA, /* Push Accumulator on Stack */
  INSN_PHP, /* Push Processor Status on Stack */
  INSN_PLA, /* Pull Accumulator from Stack */
  INSN_PLP, /* Pull Processor Status from Stack */
  INSN_RLA, /* [Undocumented]Rotate one bit left in memory, then AND accumulator with memory. */
  INSN_ROL, /* Rotate One Bit Left (Memory or Accumulator) */
  INSN_ROR, /* Rotate One Bit Right (Memory or Accumulator) */
  INSN_RRA, /* [Undocumented]Rotate one bit right in memory, then add memory to accumulator (with carry). */
  INSN_RTI, /* Return from Interrupt */
  INSN_RTS, /* Return from Subroutine */
  INSN_SBC, /* Subtract Memory from Accumulator with Borrow */
  INSN_SEC, /* Set Carry Flag */
  INSN_SED, /* Set Decimal Mode */
  INSN_SEI, /* Set Interrupt Disable Status */
  INSN_SLO, /* [Undocumented]Shift left one bit in memory, then OR accumulator with memory. */
  INSN_SRE, /* [Undocumented]Shift right one bit in memory, then EOR accumulator with memory. */
  INSN_STA, /* Store Accumulator in Memory */
  INSN_STX, /* Store Index X in Memory */
  INSN_STY, /* Store Index Y in Memory */
  INSN_SXA, /* [Undocumented]AND X register with the high byte of the target address of the argument + 1. Store the result in memory. */
  INSN_SYA, /* [Undocumented]AND Y register with the high byte of the target address of the argument + 1. Store the result in memory. */
  INSN_TAX, /* Transfer Accumulator to Index X */
  INSN_TAY, /* Transfer Accumulator to Index Y */
  INSN_TOP, /* [Undocumented]Triple NOP */
  INSN_TSX, /* Transfer Stack Pointer to Index X */
  INSN_TXA, /* Transfer Index X to Accumulator */
  INSN_TXS, /* Transfer Index X to Stack Pointer */
  INSN_TYA, /* Transfer Index Y to Accumulator */
  INSN_XAA, /* [Undocumented]A = (A | #$EE) & X & #byte */
  INSN_XAS  /* [Undocumented]AND X register with accumulator and store result in stack pointer, then
               AND stack pointer with the high byte of the target address of the argument + 1. Store result in memory. */
};

/* For the "details" field of nes6502_instruction_detail */
#define I_RD  1 /* The instruction reads from memory */
#define I_WR  2 /* The instruction writes to memory  */
#define I_JP  4 /* It is a jump/branch (or related)  */
#define I_ST  8 /* This instruction access the stack */
#define I_UN 16 /* It is an undocumented instruction */


/* For the used/changed registers in nes6502_instruction_detail */
#define R_A  1
#define R_X  2
#define R_Y  4
#define R_S  8

struct nes6502_instruction_detail {
  const char *name;
  uint32 details;
  uint32 changed_registers;
  uint32 used_registers;
  uint32 changed_flags;
  uint32 used_flags;
};


enum nes6502_addressing_mode {
  ADDR_ACCUMULATOR = 0,
  ADDR_IMMEDIATE,
  ADDR_ZERO_PAGE,
  ADDR_ZERO_PAGE_X,
  ADDR_ZERO_PAGE_Y,
  ADDR_ABSOLUTE,
  ADDR_ABSOLUTE_X,
  ADDR_ABSOLUTE_Y,
  ADDR_INDIRECT,
  ADDR_INDIRECT_X,
  ADDR_INDIRECT_Y,
  ADDR_RELATIVE,
  ADDR_IMPLIED
};

struct nes6502_opcode {
  enum nes6502_addressing_mode addressing;
  enum nes6502_instruction instruction;
  uint32 cycles;
};


extern struct nes6502_instruction_detail nes6502_instructions[];
extern struct nes6502_opcode nes6502_opcodes[];
extern int nes6502_addressing_length[];

uint32 nes6502_disassemble (uint8 *mem, uint32 offset, int count);

#endif /* __NES6502I_H */
