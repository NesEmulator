#!/usr/bin/make

CC=gcc
XA=xa
CFLAGS=	-O2 -g -Wall -ansi -pedantic
LIBS = 

EXAMPLES =  examples/test.o65 examples/hello.o65
OBJS =		main.o nes6502.o nes6502i.o utils.o
TARGET =	emulator

all:	$(TARGET) examples

$(TARGET):	$(OBJS)
	$(CC) -o $(TARGET) $(OBJS) $(LIBS)

.c.o:
	$(CC) -c -o $@ $(CFLAGS) $< 

%.o65: %.a65 
	$(XA) -o $@ $<

examples: $(EXAMPLES)

.PHONY: clean

clean:
	rm -f $(OBJS) $(TARGET)
	rm -f $(EXAMPLES)
