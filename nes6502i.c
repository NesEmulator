
#include "nes6502.h"
#include "nes6502i.h"
#include "utils.h"

/* Helper Definitions */
#define I_JS   I_JP | I_ST
#define I_RW   I_RD | I_WR
#define I_UR   I_UN | I_RD
#define I_UW   I_UN | I_WR
#define I_UM   I_RD | I_WR | I_UN
#define I_UP   I_UN | I_JP

/* Now for flags */
#define C_FLAG NES6502_C_FLAG
#define Z_FLAG NES6502_Z_FLAG
#define I_FLAG NES6502_I_FLAG
#define D_FLAG NES6502_D_FLAG
#define B_FLAG NES6502_B_FLAG
#define R_FLAG NES6502_R_FLAG
#define V_FLAG NES6502_V_FLAG
#define N_FLAG NES6502_N_FLAG


#define BI_FLAGS     B_FLAG       | I_FLAG
#define ZN_FLAGS     Z_FLAG       | N_FLAG
#define ZNV_FLAGS    ZN_FLAGS     | V_FLAG
#define CZN_FLAGS    ZN_FLAGS     | C_FLAG
#define CZNV_FLAGS   CZN_FLAGS    | V_FLAG
#define CZNVDI_FLAGS CZNV_FLAGS   | D_FLAG | I_FLAG
#define ALL_FLAGS    CZNVDI_FLAGS | B_FLAG

/* Now for registers */
#define R_AX   R_A | R_X
#define R_AXS  R_A | R_X | R_S


struct nes6502_instruction_detail nes6502_instructions[] = {
  { "AAC", I_UN , 0   , R_A  , CZN_FLAGS  , 0            },
  { "AAX", I_UW , 0   , R_AX , 0          , 0            },
  { "ADC", I_RD , R_A , R_A  , CZNV_FLAGS , C_FLAG       },
  { "AND", I_RD , R_A , R_A  , ZN_FLAGS   , 0            },
  { "ARR", I_UN , R_A , R_A  , CZNV_FLAGS , C_FLAG       },
  { "ASL", I_RW , 0   , 0    , CZN_FLAGS  , 0            },
  { "ASR", I_UN , R_A , R_A  , CZN_FLAGS  , 0            },
  { "ATX", I_UN , R_AX, R_AX , ZN_FLAGS   , 0            },
  { "AXA", I_UW , 0   , R_AX , 0          , 0            },
  { "AXS", I_UN , R_X , R_AX , CZN_FLAGS  , 0            },
  { "BCC", I_JP , 0   , 0    , 0          , C_FLAG       },
  { "BCS", I_JP , 0   , 0    , 0          , C_FLAG       },
  { "BEQ", I_JP , 0   , 0    , 0          , Z_FLAG       },
  { "BIT", I_RD , 0   , R_A  , ZNV_FLAGS  , 0            },
  { "BMI", I_JP , 0   , 0    , 0          , N_FLAG       },
  { "BNE", I_JP , 0   , 0    , 0          , Z_FLAG       },
  { "BPL", I_JP , 0   , 0    , 0          , N_FLAG       },
  { "BRK", I_JS , 0   , 0    , BI_FLAGS   , CZNVDI_FLAGS },
  { "BVC", I_JP , 0   , 0    , 0          , V_FLAG       },
  { "BVS", I_JP , 0   , 0    , 0          , V_FLAG       },
  { "CLC", 0    , 0   , 0    , C_FLAG     , 0            },
  { "CLD", 0    , 0   , 0    , D_FLAG     , 0            },
  { "CLI", 0    , 0   , 0    , I_FLAG     , 0            },
  { "CLV", 0    , 0   , 0    , V_FLAG     , 0            },
  { "CMP", I_RD , 0   , R_A  , CZN_FLAGS  , 0            },
  { "CPX", I_RD , 0   , R_X  , CZN_FLAGS  , 0            },
  { "CPY", I_RD , 0   , R_Y  , CZN_FLAGS  , 0            },
  { "DCP", I_UM , 0   , R_A  , CZN_FLAGS  , 0            },
  { "DEC", I_RW , 0   , 0    , ZN_FLAGS   , 0            },
  { "DEX", I_RW , R_X , R_X  , ZN_FLAGS   , 0            },
  { "DEY", I_RW , R_Y , R_Y  , ZN_FLAGS   , 0            },
  { "DOP", I_UR , 0   , 0    , 0          , 0            },
  { "EOR", I_RD , R_A , R_A  , ZN_FLAGS   , 0            },
  { "HLT", I_UP , 0   , 0    , 0          , 0            },
  { "INC", I_RW , 0   , 0    , ZN_FLAGS   , 0            },
  { "INX", I_RW , R_X , R_X  , ZN_FLAGS   , 0            },
  { "INY", I_RW , R_Y , R_Y  , ZN_FLAGS   , 0            },
  { "ISC", I_UM , R_A , R_A  , CZNV_FLAGS , C_FLAG       },
  { "JMP", I_JP , 0   , 0    , 0          , 0            },
  { "JSR", I_JS , 0   , 0    , 0          , 0            },
  { "LAR", I_UR , R_AXS, R_S , ZN_FLAGS   , 0            },
  { "LAX", I_UR , R_AX, 0    , ZN_FLAGS   , 0            },
  { "LDA", I_RD , R_A , 0    , ZN_FLAGS   , 0            },
  { "LDX", I_RD , R_X , 0    , ZN_FLAGS   , 0            },
  { "LDY", I_RD , R_Y , 0    , ZN_FLAGS   , 0            },
  { "LSR", I_RW , 0   , 0    , CZN_FLAGS  , 0            },
  { "NOP", 0    , 0   , 0    , 0          , 0            },
  { "ORA", I_RD , R_A , R_A  , ZN_FLAGS   , 0            },
  { "PHA", I_ST , 0   , R_A  , 0          , 0            },
  { "PHP", I_ST , 0   , 0    , 0          , ALL_FLAGS    },
  { "PLA", I_ST , R_A , 0    , ZN_FLAGS   , 0            },
  { "PLP", I_ST , 0   , 0    , ALL_FLAGS  , 0            },
  { "RLA", I_UM , R_A , R_A  , CZN_FLAGS  , C_FLAG       },
  { "ROL", I_RW , 0   , 0    , CZN_FLAGS  , C_FLAG       },
  { "ROR", I_RW , 0   , 0    , CZN_FLAGS  , C_FLAG       },
  { "RRA", I_UM , R_A , R_A  , CZNV_FLAGS , C_FLAG       },
  { "RTI", I_JS , 0   , 0    , ALL_FLAGS  , 0            },
  { "RTS", I_JS , 0   , 0    , 0          , 0            },
  { "SBC", I_RD , R_A , R_A  , CZNV_FLAGS , C_FLAG       },
  { "SEC", 0    , 0   , 0    , C_FLAG     , 0            },
  { "SED", 0    , 0   , 0    , D_FLAG     , 0            },
  { "SEI", 0    , 0   , 0    , I_FLAG     , 0            },
  { "SLO", I_UM , R_A , R_A  , CZN_FLAGS  , 0            },
  { "SRE", I_UM , R_A , R_A  , CZN_FLAGS  , 0            },
  { "STA", I_WR , 0   , R_A  , 0          , 0            },
  { "STX", I_WR , 0   , R_X  , 0          , 0            },
  { "STY", I_WR , 0   , R_Y  , 0          , 0            },
  { "SXA", I_UW , 0   , R_X  , 0          , 0            },
  { "SYA", I_UW , 0   , R_Y  , 0          , 0            },
  { "TAX", 0    , R_X , R_A  , ZN_FLAGS   , 0            },
  { "TAY", 0    , R_Y , R_A  , ZN_FLAGS   , 0            },
  { "TOP", I_UR , 0   , 0    , 0          , 0            },
  { "TSX", 0    , R_X , R_S  , ZN_FLAGS   , 0            },
  { "TXA", 0    , R_A , R_X  , ZN_FLAGS   , 0            },
  { "TXS", 0    , R_S , R_X  , 0          , 0            },
  { "TYA", 0    , R_A , R_Y  , ZN_FLAGS   , 0            },
  { "XAA", I_UN , R_A , R_AX , ZN_FLAGS   , 0            },
  { "XAS", I_UW , R_S , R_AX , 0          , 0            }
};


int nes6502_addressing_length[] = {
  /* ADDR_ACCUMULATOR */  1,
  /* ADDR_IMMEDIATE   */  2,
  /* ADDR_ZERO_PAGE   */  2,
  /* ADDR_ZERO_PAGE_X */  2,
  /* ADDR_ZERO_PAGE_Y */  2,
  /* ADDR_ABSOLUTE    */  3,
  /* ADDR_ABSOLUTE_X  */  3,
  /* ADDR_ABSOLUTE_Y  */  3,
  /* ADDR_INDIRECT    */  3,
  /* ADDR_INDIRECT_X  */  2,
  /* ADDR_INDIRECT_Y  */  2,
  /* ADDR_RELATIVE    */  2,
  /* ADDR_IMPLIED     */  1
};

struct nes6502_opcode nes6502_opcodes[] = {
  {/* 0x00 */ ADDR_IMPLIED     , INSN_BRK, 7},
  {/* 0x01 */ ADDR_INDIRECT_X  , INSN_ORA, 6},
  {/* 0x02 */ ADDR_IMPLIED     , INSN_HLT, 0},
  {/* 0x03 */ ADDR_INDIRECT_X  , INSN_SLO, 8},
  {/* 0x04 */ ADDR_ZERO_PAGE   , INSN_DOP, 3},
  {/* 0x05 */ ADDR_ZERO_PAGE   , INSN_ORA, 3},
  {/* 0x06 */ ADDR_ZERO_PAGE   , INSN_ASL, 5},
  {/* 0x07 */ ADDR_ZERO_PAGE   , INSN_SLO, 5},
  {/* 0x08 */ ADDR_IMPLIED     , INSN_PHP, 3},
  {/* 0x09 */ ADDR_IMMEDIATE   , INSN_ORA, 2},
  {/* 0x0a */ ADDR_ACCUMULATOR , INSN_ASL, 2},
  {/* 0x0b */ ADDR_IMMEDIATE   , INSN_AAC, 2},
  {/* 0x0c */ ADDR_ABSOLUTE    , INSN_TOP, 4},
  {/* 0x0d */ ADDR_ABSOLUTE    , INSN_ORA, 4},
  {/* 0x0e */ ADDR_ABSOLUTE    , INSN_ASL, 6},
  {/* 0x0f */ ADDR_ABSOLUTE    , INSN_SLO, 6},
  {/* 0x10 */ ADDR_RELATIVE    , INSN_BPL, 2},
  {/* 0x11 */ ADDR_INDIRECT_Y  , INSN_ORA, 5},
  {/* 0x12 */ ADDR_IMPLIED     , INSN_HLT, 0},
  {/* 0x13 */ ADDR_INDIRECT_Y  , INSN_SLO, 8},
  {/* 0x14 */ ADDR_ZERO_PAGE_X , INSN_DOP, 4},
  {/* 0x15 */ ADDR_ZERO_PAGE_X , INSN_ORA, 4},
  {/* 0x16 */ ADDR_ZERO_PAGE_X , INSN_ASL, 6},
  {/* 0x17 */ ADDR_ZERO_PAGE_X , INSN_SLO, 6},
  {/* 0x18 */ ADDR_IMPLIED     , INSN_CLC, 2},
  {/* 0x19 */ ADDR_ABSOLUTE_Y  , INSN_ORA, 4},
  {/* 0x1a */ ADDR_IMPLIED     , INSN_NOP, 2},
  {/* 0x1b */ ADDR_ABSOLUTE_Y  , INSN_SLO, 7},
  {/* 0x1c */ ADDR_ABSOLUTE_X  , INSN_TOP, 4},
  {/* 0x1d */ ADDR_ABSOLUTE_X  , INSN_ORA, 4},
  {/* 0x1e */ ADDR_ABSOLUTE_X  , INSN_ASL, 7},
  {/* 0x1f */ ADDR_ABSOLUTE_X  , INSN_SLO, 7},
  {/* 0x20 */ ADDR_ABSOLUTE    , INSN_JSR, 6},
  {/* 0x21 */ ADDR_INDIRECT_X  , INSN_AND, 6},
  {/* 0x22 */ ADDR_IMPLIED     , INSN_HLT, 0},
  {/* 0x23 */ ADDR_INDIRECT_X  , INSN_RLA, 8},
  {/* 0x24 */ ADDR_ZERO_PAGE   , INSN_BIT, 3},
  {/* 0x25 */ ADDR_ZERO_PAGE   , INSN_AND, 3},
  {/* 0x26 */ ADDR_ZERO_PAGE   , INSN_ROL, 5},
  {/* 0x27 */ ADDR_ZERO_PAGE   , INSN_RLA, 5},
  {/* 0x28 */ ADDR_IMPLIED     , INSN_PLP, 4},
  {/* 0x29 */ ADDR_IMMEDIATE   , INSN_AND, 2},
  {/* 0x2a */ ADDR_ACCUMULATOR , INSN_ROL, 2},
  {/* 0x2b */ ADDR_IMMEDIATE   , INSN_AAC, 2},
  {/* 0x2c */ ADDR_ABSOLUTE    , INSN_BIT, 4},
  {/* 0x2d */ ADDR_ABSOLUTE    , INSN_AND, 4},
  {/* 0x2e */ ADDR_ABSOLUTE    , INSN_ROL, 6},
  {/* 0x2f */ ADDR_ABSOLUTE    , INSN_RLA, 6},
  {/* 0x30 */ ADDR_RELATIVE    , INSN_BMI, 2},
  {/* 0x31 */ ADDR_INDIRECT_Y  , INSN_AND, 5},
  {/* 0x32 */ ADDR_IMPLIED     , INSN_HLT, 0},
  {/* 0x33 */ ADDR_INDIRECT_Y  , INSN_RLA, 8},
  {/* 0x34 */ ADDR_ZERO_PAGE_X , INSN_DOP, 4},
  {/* 0x35 */ ADDR_ZERO_PAGE_X , INSN_AND, 4},
  {/* 0x36 */ ADDR_ZERO_PAGE_X , INSN_ROL, 6},
  {/* 0x37 */ ADDR_ZERO_PAGE_X , INSN_RLA, 6},
  {/* 0x38 */ ADDR_IMPLIED     , INSN_SEC, 2},
  {/* 0x39 */ ADDR_ABSOLUTE_Y  , INSN_AND, 4},
  {/* 0x3a */ ADDR_IMPLIED     , INSN_NOP, 2},
  {/* 0x3b */ ADDR_ABSOLUTE_Y  , INSN_RLA, 7},
  {/* 0x3c */ ADDR_ABSOLUTE_X  , INSN_TOP, 4},
  {/* 0x3d */ ADDR_ABSOLUTE_X  , INSN_AND, 4},
  {/* 0x3e */ ADDR_ABSOLUTE_X  , INSN_ROL, 7},
  {/* 0x3f */ ADDR_ABSOLUTE_X  , INSN_RLA, 7},
  {/* 0x40 */ ADDR_IMPLIED     , INSN_RTI, 6},
  {/* 0x41 */ ADDR_INDIRECT_X  , INSN_EOR, 6},
  {/* 0x42 */ ADDR_IMPLIED     , INSN_HLT, 0},
  {/* 0x43 */ ADDR_INDIRECT_X  , INSN_SRE, 8},
  {/* 0x44 */ ADDR_ZERO_PAGE   , INSN_DOP, 3},
  {/* 0x45 */ ADDR_ZERO_PAGE   , INSN_EOR, 3},
  {/* 0x46 */ ADDR_ZERO_PAGE   , INSN_LSR, 5},
  {/* 0x47 */ ADDR_ZERO_PAGE   , INSN_SRE, 5},
  {/* 0x48 */ ADDR_IMPLIED     , INSN_PHA, 3},
  {/* 0x49 */ ADDR_IMMEDIATE   , INSN_EOR, 2},
  {/* 0x4a */ ADDR_ACCUMULATOR , INSN_LSR, 2},
  {/* 0x4b */ ADDR_IMMEDIATE   , INSN_ASR, 2},
  {/* 0x4c */ ADDR_ABSOLUTE    , INSN_JMP, 3},
  {/* 0x4d */ ADDR_ABSOLUTE    , INSN_EOR, 4},
  {/* 0x4e */ ADDR_ABSOLUTE    , INSN_LSR, 6},
  {/* 0x4f */ ADDR_ABSOLUTE    , INSN_SRE, 6},
  {/* 0x50 */ ADDR_RELATIVE    , INSN_BVC, 2},
  {/* 0x51 */ ADDR_INDIRECT_Y  , INSN_EOR, 5},
  {/* 0x52 */ ADDR_IMPLIED     , INSN_HLT, 0},
  {/* 0x53 */ ADDR_INDIRECT_Y  , INSN_SRE, 8},
  {/* 0x54 */ ADDR_ZERO_PAGE_X , INSN_DOP, 4},
  {/* 0x55 */ ADDR_ZERO_PAGE_X , INSN_EOR, 4},
  {/* 0x56 */ ADDR_ZERO_PAGE_X , INSN_LSR, 6},
  {/* 0x57 */ ADDR_ZERO_PAGE_X , INSN_SRE, 6},
  {/* 0x58 */ ADDR_IMPLIED     , INSN_CLI, 2},
  {/* 0x59 */ ADDR_ABSOLUTE_Y  , INSN_EOR, 4},
  {/* 0x5a */ ADDR_IMPLIED     , INSN_NOP, 2},
  {/* 0x5b */ ADDR_ABSOLUTE_Y  , INSN_SRE, 7},
  {/* 0x5c */ ADDR_ABSOLUTE_X  , INSN_TOP, 4},
  {/* 0x5d */ ADDR_ABSOLUTE_X  , INSN_EOR, 4},
  {/* 0x5e */ ADDR_ABSOLUTE_X  , INSN_LSR, 7},
  {/* 0x5f */ ADDR_ABSOLUTE_X  , INSN_SRE, 7},
  {/* 0x60 */ ADDR_IMPLIED     , INSN_RTS, 6},
  {/* 0x61 */ ADDR_INDIRECT_X  , INSN_ADC, 6},
  {/* 0x62 */ ADDR_IMPLIED     , INSN_HLT, 0},
  {/* 0x63 */ ADDR_INDIRECT_X  , INSN_RRA, 8},
  {/* 0x64 */ ADDR_ZERO_PAGE   , INSN_DOP, 3},
  {/* 0x65 */ ADDR_ZERO_PAGE   , INSN_ADC, 3},
  {/* 0x66 */ ADDR_ZERO_PAGE   , INSN_ROR, 5},
  {/* 0x67 */ ADDR_ZERO_PAGE   , INSN_RRA, 5},
  {/* 0x68 */ ADDR_IMPLIED     , INSN_PLA, 4},
  {/* 0x69 */ ADDR_IMMEDIATE   , INSN_ADC, 2},
  {/* 0x6a */ ADDR_ACCUMULATOR , INSN_ROR, 2},
  {/* 0x6b */ ADDR_IMMEDIATE   , INSN_ARR, 2},
  {/* 0x6c */ ADDR_INDIRECT    , INSN_JMP, 5},
  {/* 0x6d */ ADDR_ABSOLUTE    , INSN_ADC, 4},
  {/* 0x6e */ ADDR_ABSOLUTE    , INSN_ROR, 6},
  {/* 0x6f */ ADDR_ABSOLUTE    , INSN_RRA, 6},
  {/* 0x70 */ ADDR_RELATIVE    , INSN_BVS, 2},
  {/* 0x71 */ ADDR_INDIRECT_Y  , INSN_ADC, 5},
  {/* 0x72 */ ADDR_IMPLIED     , INSN_HLT, 0},
  {/* 0x73 */ ADDR_INDIRECT_Y  , INSN_RRA, 8},
  {/* 0x74 */ ADDR_ZERO_PAGE_X , INSN_DOP, 4},
  {/* 0x75 */ ADDR_ZERO_PAGE_X , INSN_ADC, 4},
  {/* 0x76 */ ADDR_ZERO_PAGE_X , INSN_ROR, 6},
  {/* 0x77 */ ADDR_ZERO_PAGE_X , INSN_RRA, 6},
  {/* 0x78 */ ADDR_IMPLIED     , INSN_SEI, 2},
  {/* 0x79 */ ADDR_ABSOLUTE_Y  , INSN_ADC, 4},
  {/* 0x7a */ ADDR_IMPLIED     , INSN_NOP, 2},
  {/* 0x7b */ ADDR_ABSOLUTE_Y  , INSN_RRA, 7},
  {/* 0x7c */ ADDR_ABSOLUTE_X  , INSN_TOP, 4},
  {/* 0x7d */ ADDR_ABSOLUTE_X  , INSN_ADC, 4},
  {/* 0x7e */ ADDR_ABSOLUTE_X  , INSN_ROR, 7},
  {/* 0x7f */ ADDR_ABSOLUTE_X  , INSN_RRA, 7},
  {/* 0x80 */ ADDR_IMMEDIATE   , INSN_DOP, 2},
  {/* 0x81 */ ADDR_INDIRECT_X  , INSN_STA, 6},
  {/* 0x82 */ ADDR_IMMEDIATE   , INSN_DOP, 2},
  {/* 0x83 */ ADDR_INDIRECT_X  , INSN_AAX, 6},
  {/* 0x84 */ ADDR_ZERO_PAGE   , INSN_STY, 3},
  {/* 0x85 */ ADDR_ZERO_PAGE   , INSN_STA, 3},
  {/* 0x86 */ ADDR_ZERO_PAGE   , INSN_STX, 3},
  {/* 0x87 */ ADDR_ZERO_PAGE   , INSN_AAX, 3},
  {/* 0x88 */ ADDR_IMPLIED     , INSN_DEY, 2},
  {/* 0x89 */ ADDR_IMMEDIATE   , INSN_DOP, 2},
  {/* 0x8a */ ADDR_IMPLIED     , INSN_TXA, 2},
  {/* 0x8b */ ADDR_IMMEDIATE   , INSN_XAA, 2},
  {/* 0x8c */ ADDR_ABSOLUTE    , INSN_STY, 4},
  {/* 0x8d */ ADDR_ABSOLUTE    , INSN_STA, 4},
  {/* 0x8e */ ADDR_ABSOLUTE    , INSN_STX, 4},
  {/* 0x8f */ ADDR_ABSOLUTE    , INSN_AAX, 4},
  {/* 0x90 */ ADDR_RELATIVE    , INSN_BCC, 2},
  {/* 0x91 */ ADDR_INDIRECT_Y  , INSN_STA, 6},
  {/* 0x92 */ ADDR_IMPLIED     , INSN_HLT, 0},
  {/* 0x93 */ ADDR_INDIRECT_Y  , INSN_AXA, 6},
  {/* 0x94 */ ADDR_ZERO_PAGE_X , INSN_STY, 4},
  {/* 0x95 */ ADDR_ZERO_PAGE_X , INSN_STA, 4},
  {/* 0x96 */ ADDR_ZERO_PAGE_Y , INSN_STX, 4},
  {/* 0x97 */ ADDR_ZERO_PAGE_Y , INSN_AAX, 4},
  {/* 0x98 */ ADDR_IMPLIED     , INSN_TYA, 2},
  {/* 0x99 */ ADDR_ABSOLUTE_Y  , INSN_STA, 5},
  {/* 0x9a */ ADDR_IMPLIED     , INSN_TXS, 2},
  {/* 0x9b */ ADDR_ABSOLUTE_Y  , INSN_XAS, 5},
  {/* 0x9c */ ADDR_ABSOLUTE_X  , INSN_SYA, 5},
  {/* 0x9d */ ADDR_ABSOLUTE_X  , INSN_STA, 5},
  {/* 0x9e */ ADDR_ABSOLUTE_Y  , INSN_SXA, 5},
  {/* 0x9f */ ADDR_ABSOLUTE_Y  , INSN_AXA, 5},
  {/* 0xa0 */ ADDR_IMMEDIATE   , INSN_LDY, 2},
  {/* 0xa1 */ ADDR_INDIRECT_X  , INSN_LDA, 6},
  {/* 0xa2 */ ADDR_IMMEDIATE   , INSN_LDX, 2},
  {/* 0xa3 */ ADDR_INDIRECT_X  , INSN_LAX, 6},
  {/* 0xa4 */ ADDR_ZERO_PAGE   , INSN_LDY, 3},
  {/* 0xa5 */ ADDR_ZERO_PAGE   , INSN_LDA, 3},
  {/* 0xa6 */ ADDR_ZERO_PAGE   , INSN_LDX, 3},
  {/* 0xa7 */ ADDR_ZERO_PAGE   , INSN_LAX, 3},
  {/* 0xa8 */ ADDR_IMPLIED     , INSN_TAY, 2},
  {/* 0xa9 */ ADDR_IMMEDIATE   , INSN_LDA, 2},
  {/* 0xaa */ ADDR_IMPLIED     , INSN_TAX, 2},
  {/* 0xab */ ADDR_IMMEDIATE   , INSN_ATX, 2},
  {/* 0xac */ ADDR_ABSOLUTE    , INSN_LDY, 4},
  {/* 0xad */ ADDR_ABSOLUTE    , INSN_LDA, 4},
  {/* 0xae */ ADDR_ABSOLUTE    , INSN_LDX, 4},
  {/* 0xaf */ ADDR_ABSOLUTE    , INSN_LAX, 4},
  {/* 0xb0 */ ADDR_RELATIVE    , INSN_BCS, 2},
  {/* 0xb1 */ ADDR_INDIRECT_Y  , INSN_LDA, 5},
  {/* 0xb2 */ ADDR_IMPLIED     , INSN_HLT, 0},
  {/* 0xb3 */ ADDR_INDIRECT_Y  , INSN_LAX, 5},
  {/* 0xb4 */ ADDR_ZERO_PAGE_X , INSN_LDY, 4},
  {/* 0xb5 */ ADDR_ZERO_PAGE_X , INSN_LDA, 4},
  {/* 0xb6 */ ADDR_ZERO_PAGE_Y , INSN_LDX, 4},
  {/* 0xb7 */ ADDR_ZERO_PAGE_Y , INSN_LAX, 4},
  {/* 0xb8 */ ADDR_IMPLIED     , INSN_CLV, 2},
  {/* 0xb9 */ ADDR_ABSOLUTE_Y  , INSN_LDA, 4},
  {/* 0xba */ ADDR_IMPLIED     , INSN_TSX, 2},
  {/* 0xbb */ ADDR_ABSOLUTE_Y  , INSN_LAR, 4},
  {/* 0xbc */ ADDR_ABSOLUTE_X  , INSN_LDY, 4},
  {/* 0xbd */ ADDR_ABSOLUTE_X  , INSN_LDA, 4},
  {/* 0xbe */ ADDR_ABSOLUTE_Y  , INSN_LDX, 4},
  {/* 0xbf */ ADDR_ABSOLUTE_Y  , INSN_LAX, 4},
  {/* 0xc0 */ ADDR_IMMEDIATE   , INSN_CPY, 2},
  {/* 0xc1 */ ADDR_INDIRECT_X  , INSN_CMP, 6},
  {/* 0xc2 */ ADDR_IMMEDIATE   , INSN_DOP, 2},
  {/* 0xc3 */ ADDR_INDIRECT_X  , INSN_DCP, 8},
  {/* 0xc4 */ ADDR_ZERO_PAGE   , INSN_CPY, 3},
  {/* 0xc5 */ ADDR_ZERO_PAGE   , INSN_CMP, 3},
  {/* 0xc6 */ ADDR_ZERO_PAGE   , INSN_DEC, 5},
  {/* 0xc7 */ ADDR_ZERO_PAGE   , INSN_DCP, 5},
  {/* 0xc8 */ ADDR_IMPLIED     , INSN_INY, 2},
  {/* 0xc9 */ ADDR_IMMEDIATE   , INSN_CMP, 2},
  {/* 0xca */ ADDR_IMPLIED     , INSN_DEX, 2},
  {/* 0xcb */ ADDR_IMMEDIATE   , INSN_AXS, 2},
  {/* 0xcc */ ADDR_ABSOLUTE    , INSN_CPY, 4},
  {/* 0xcd */ ADDR_ABSOLUTE    , INSN_CMP, 4},
  {/* 0xce */ ADDR_ABSOLUTE    , INSN_DEC, 6},
  {/* 0xcf */ ADDR_ABSOLUTE    , INSN_DCP, 6},
  {/* 0xd0 */ ADDR_RELATIVE    , INSN_BNE, 2},
  {/* 0xd1 */ ADDR_INDIRECT_Y  , INSN_CMP, 5},
  {/* 0xd2 */ ADDR_IMPLIED     , INSN_HLT, 0},
  {/* 0xd3 */ ADDR_INDIRECT_Y  , INSN_DCP, 8},
  {/* 0xd4 */ ADDR_ZERO_PAGE_X , INSN_DOP, 4},
  {/* 0xd5 */ ADDR_ZERO_PAGE_X , INSN_CMP, 4},
  {/* 0xd6 */ ADDR_ZERO_PAGE_X , INSN_DEC, 6},
  {/* 0xd7 */ ADDR_ZERO_PAGE_X , INSN_DCP, 6},
  {/* 0xd8 */ ADDR_IMPLIED     , INSN_CLD, 2},
  {/* 0xd9 */ ADDR_ABSOLUTE_Y  , INSN_CMP, 4},
  {/* 0xda */ ADDR_IMPLIED     , INSN_NOP, 2},
  {/* 0xdb */ ADDR_ABSOLUTE_Y  , INSN_DCP, 7},
  {/* 0xdc */ ADDR_ABSOLUTE_X  , INSN_TOP, 4},
  {/* 0xdd */ ADDR_ABSOLUTE_X  , INSN_CMP, 4},
  {/* 0xde */ ADDR_ABSOLUTE_X  , INSN_DEC, 7},
  {/* 0xdf */ ADDR_ABSOLUTE_X  , INSN_DCP, 7},
  {/* 0xe0 */ ADDR_IMMEDIATE   , INSN_CPX, 2},
  {/* 0xe1 */ ADDR_INDIRECT_X  , INSN_SBC, 6},
  {/* 0xe2 */ ADDR_IMMEDIATE   , INSN_DOP, 2},
  {/* 0xe3 */ ADDR_INDIRECT_X  , INSN_ISC, 8},
  {/* 0xe4 */ ADDR_ZERO_PAGE   , INSN_CPX, 3},
  {/* 0xe5 */ ADDR_ZERO_PAGE   , INSN_SBC, 3},
  {/* 0xe6 */ ADDR_ZERO_PAGE   , INSN_INC, 5},
  {/* 0xe7 */ ADDR_ZERO_PAGE   , INSN_ISC, 5},
  {/* 0xe8 */ ADDR_IMPLIED     , INSN_INX, 2},
  {/* 0xe9 */ ADDR_IMMEDIATE   , INSN_SBC, 2},
  {/* 0xea */ ADDR_IMPLIED     , INSN_NOP, 2},
  {/* 0xeb */ ADDR_IMMEDIATE   , INSN_SBC, 2},
  {/* 0xec */ ADDR_ABSOLUTE    , INSN_CPX, 4},
  {/* 0xed */ ADDR_ABSOLUTE    , INSN_SBC, 4},
  {/* 0xee */ ADDR_ABSOLUTE    , INSN_INC, 6},
  {/* 0xef */ ADDR_ABSOLUTE    , INSN_ISC, 6},
  {/* 0xf0 */ ADDR_RELATIVE    , INSN_BEQ, 2},
  {/* 0xf1 */ ADDR_INDIRECT_Y  , INSN_SBC, 5},
  {/* 0xf2 */ ADDR_IMPLIED     , INSN_HLT, 0},
  {/* 0xf3 */ ADDR_INDIRECT_Y  , INSN_ISC, 8},
  {/* 0xf4 */ ADDR_ZERO_PAGE_X , INSN_DOP, 4},
  {/* 0xf5 */ ADDR_ZERO_PAGE_X , INSN_SBC, 4},
  {/* 0xf6 */ ADDR_ZERO_PAGE_X , INSN_INC, 6},
  {/* 0xf7 */ ADDR_ZERO_PAGE_X , INSN_ISC, 6},
  {/* 0xf8 */ ADDR_IMPLIED     , INSN_SED, 2},
  {/* 0xf9 */ ADDR_ABSOLUTE_Y  , INSN_SBC, 4},
  {/* 0xfa */ ADDR_IMPLIED     , INSN_NOP, 2},
  {/* 0xfb */ ADDR_ABSOLUTE_Y  , INSN_ISC, 7},
  {/* 0xfc */ ADDR_ABSOLUTE_X  , INSN_TOP, 4},
  {/* 0xfd */ ADDR_ABSOLUTE_X  , INSN_SBC, 4},
  {/* 0xfe */ ADDR_ABSOLUTE_X  , INSN_INC, 7},
  {/* 0xff */ ADDR_ABSOLUTE_X  , INSN_ISC, 7}
};


uint32 nes6502_disassemble (uint8 *mem, uint32 offset, int count)
{
  uint32 pos = 0;
  while (count--) {
    struct nes6502_opcode *opcode;
    enum nes6502_instruction insn;
    uint32 oper, address;
    uint8 opc;

    opc = mem[pos++];
    opcode = &nes6502_opcodes[opc];
    insn = opcode->instruction;

    report ("$%04X: [ $%02X ", offset, opc);
    switch (nes6502_addressing_length[opcode->addressing]) {
      case 1: report ("]           "); break;
      case 2: report ("$%02X ]       ", mem[pos]); break;
      case 3: report ("$%02X $%02X ]   ", mem[pos], mem[pos + 1]); break;
    }

    report ("%s ", nes6502_instructions[insn].name);
    offset += nes6502_addressing_length[opcode->addressing];

    switch (opcode->addressing) {
      case ADDR_ACCUMULATOR:
        report (" A\n");
        break;
      case ADDR_IMMEDIATE:
        report ("#$%02X\n", mem[pos++]);
        break;
      case ADDR_ZERO_PAGE:
        report (" $%02X\n", mem[pos++]);
        break;
      case ADDR_ZERO_PAGE_X:
        report (" $%02X, X\n", mem[pos++]);
        break;
      case ADDR_ZERO_PAGE_Y:
        report (" $%02X, Y\n", mem[pos++]);
        break;
      case ADDR_ABSOLUTE:
        oper = (uint32) mem[pos++];
        oper += ((uint32) mem[pos++]) << 8;
        report (" $%04X\n", oper);
        break;
      case ADDR_ABSOLUTE_X:
        oper = (uint32) mem[pos++];
        oper += ((uint32) mem[pos++]) << 8;
        report (" $%04X, X\n", oper);
        break;
      case ADDR_ABSOLUTE_Y:
        oper = (uint32) mem[pos++];
        oper += ((uint32) mem[pos++]) << 8;
        report (" $%04X, Y\n", oper);
        break;
      case ADDR_INDIRECT:
        oper = (uint32) mem[pos++];
        oper += ((uint32) mem[pos++]) << 8;
        report ("($%04X)\n", oper);
        break;
      case ADDR_INDIRECT_X:
        report ("($%02X, X)\n", mem[pos++]);
        break;
      case ADDR_INDIRECT_Y:
        report ("($%02X), Y\n", mem[pos++]);
        break;
      case ADDR_RELATIVE:
        address = mem[pos++];
        if (address & 0x80) address |= 0xFF00;
        address = (address + offset) & 0xFFFF;
        report (" $%04X\n", address);
        break;
      case ADDR_IMPLIED:
        report ("\n");
        break;
    }
  }
  return pos;
}


#ifdef TEST_DISASSEMBLE
int main (int argc, char **argv)
{
  size_t size;
  uint32 offset = 0, pos = 0;
  uint8 *mem;

  if (argc <= 1) return -1;
  if (argc > 2) offset = atoi (argv[2]);
  mem = (uint8 *) read_file (argv[1], &size);

  while (pos < size) {
    pos += nes6502_disassemble (&mem[pos], offset + pos, 1);
  }
  return 0;
}
#endif /* TEST_DISASSEMBLE */

