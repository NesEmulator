#ifndef __DYNREC_H
#define __DYNREC_H

void dynrec_init (void);
void dynrec_exec (int cycles);

#endif /* __DYNREC_H */
