
#include "dynrec.h"
#include "nes6502.h"
#include "nes6502i.h"
#include "types.h"
#include "utils.h"

#define DYNREC_MEM_SIZE 65536

nes6502_context dynrec_cpu;
void **dynrec_code;
int   *dynrec_pc;
int   *dynrec_cycles;

extern void dynrec_start_exec (void);
extern void dynrec_no_op (void);

void dynrec_init (void)
{
  int i;
  dynrec_code = xmalloc (DYNREC_MEM_SIZE * sizeof (void *));
  dynrec_pc = xmalloc (DYNREC_MEM_SIZE * sizeof (int));
  dynrec_cycles = xmalloc (DYNREC_MEM_SIZE * sizeof (int));

  for (i = 0; i < DYNREC_MEM_SIZE; i++) {
    dynrec_code[i] = &dynrec_no_op;
    dynrec_pc[i] = 1;
    dynrec_cycles[i] = 1;
  }
}

void dynrec_exec (int cycles)
{
  dynrec_cpu.remaining_cycles = cycles;
  dynrec_start_exec ();

  dynrec_cpu.total_cycles += cycles - dynrec_cpu.remaining_cycles;
}

static
void dynrec_printcontext (void)
{
  report ("A=$%02X X=$%02X Y=$%02X SP=$%02X PC=$%04X ",
      dynrec_cpu.a_reg, dynrec_cpu.x_reg, dynrec_cpu.y_reg, dynrec_cpu.s_reg, dynrec_cpu.pc_reg);
  report ((dynrec_cpu.p_reg & NES6502_C_FLAG) ? "C" : "c");
  report ((dynrec_cpu.p_reg & NES6502_Z_FLAG) ? "Z" : "z");
  report ((dynrec_cpu.p_reg & NES6502_V_FLAG) ? "V" : "v");
  report ((dynrec_cpu.p_reg & NES6502_N_FLAG) ? "N" : "n");
  report ((dynrec_cpu.p_reg & NES6502_I_FLAG) ? "I" : "i");
  report ((dynrec_cpu.p_reg & NES6502_B_FLAG) ? "B" : "b");
  report ((dynrec_cpu.p_reg & NES6502_D_FLAG) ? "D" : "d");
  report ("($%02X) cycles=$%08X", dynrec_cpu.p_reg, dynrec_cpu.total_cycles);
  if (dynrec_cpu.halted) report (" (halted)");
  report ("\n");
}

#ifdef _PSP_FW_VERSION
#include <pspkernel.h>
#include <pspmoduleinfo.h>

/* Define the module info section */
PSP_MODULE_INFO ("emulator", 0, 1, 1);
PSP_HEAP_SIZE_KB (1024);

#endif

int main (int argc, char **argv)
{
  dynrec_init ();
  dynrec_printcontext ();
  dynrec_exec (32);
  dynrec_printcontext ();
  return 0;
}
