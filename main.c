#include <ctype.h>
#include <stdlib.h>

#include "nes6502.h"
#include "nes6502i.h"
#include "utils.h"
#include "types.h"

#ifdef _PSP_FW_VERSION
#include <pspkernel.h>
#include <pspmoduleinfo.h>

/* Define the module info section */
PSP_MODULE_INFO ("emulator", 0, 1, 1);
PSP_HEAP_SIZE_KB (1024);

#endif

#define IO_CMD_PORT     0x4003
#define IO_PARAMHI_PORT 0x4002
#define IO_PARAMLO_PORT 0x4001

#define CMD_EXIT      0xFF
#define CMD_CPU       0xFE
#define CMD_DUMP      0xFD
#define CMD_IRQ       0xFC
#define CMD_NMI       0xFB
#define CMD_RESET     0xFA
#define CMD_PRINT     0x01
#define CMD_PRINT_INT 0x02
#define CMD_PRINT_HEX 0x03

static
void nes6502_unassemble (uint32 address, int count)
{
  uint8 buf[3];
  uint32 offset = 0;
  while (count--) {
    buf[0] = nes6502_getbyte (address + offset);
    buf[1] = nes6502_getbyte (address + offset + 1);
    buf[2] = nes6502_getbyte (address + offset + 2);
    offset += nes6502_disassemble (buf, address + offset, 1);
  }
}

static
void nes6502_printcontext (void)
{
  nes6502_context cpu;
  nes6502_getcontext (&cpu);
  report ("A=$%02X X=$%02X Y=$%02X SP=$%02X ",
      cpu.a_reg, cpu.x_reg, cpu.y_reg, cpu.s_reg);
  report ((cpu.p_reg & NES6502_C_FLAG) ? "C" : "c");
  report ((cpu.p_reg & NES6502_Z_FLAG) ? "Z" : "z");
  report ((cpu.p_reg & NES6502_V_FLAG) ? "V" : "v");
  report ((cpu.p_reg & NES6502_N_FLAG) ? "N" : "n");
  report ((cpu.p_reg & NES6502_I_FLAG) ? "I" : "i");
  report ((cpu.p_reg & NES6502_B_FLAG) ? "B" : "b");
  report ((cpu.p_reg & NES6502_D_FLAG) ? "D" : "d");
  report ("($%02X) cycles=$%08X", cpu.p_reg, cpu.total_cycles);
  if (cpu.halted) report (" (halted)");
  report ("\n");

  nes6502_unassemble (cpu.pc_reg, 1);
  report ("\n");
}

static
void nes6502_dump (uint32 address, int count)
{
  report ("\n");
  while (count > 0) {
    int i, cols = MIN (count, 16);
    report ("$%04X: ", address);
    for (i = 0; i < cols; i++) {
      report (" %02X", nes6502_getbyte (address++));
    }
    for (; i < 16; i++) {
      report ("   ");
    }
    report (" ");
    address -= cols;
    for (i = 0; i < cols; i++) {
      uint8 c = nes6502_getbyte (address++);
      if (isprint (c)) {
        report ("%c", (char) c);
      } else {
        report (".");
      }
    }
    report ("\n");
    count -= cols;
  }
}

static
uint8 read_func (uint32 address)
{
  report ("Reading from $%04X", address);
  return 0;
}

static uint32 nes6502_cmd_param;

static
void write_func (uint32 address, uint8 value)
{
  if (address == IO_CMD_PORT) {
    switch (value) {
      case CMD_EXIT:
        nes6502_halt ();
        break;
      case CMD_PRINT:
        address = nes6502_cmd_param;
        while (1) {
          uint8 c = nes6502_getbyte (address++);
          if (c == 0) break;
          report ("%c", c);
        }
        break;
      case CMD_PRINT_INT:
        report ("%d", (int) nes6502_cmd_param);
        break;
      case CMD_PRINT_HEX:
        report ("$%04X", nes6502_cmd_param);
        break;
      case CMD_CPU:
        nes6502_printcontext ();
        break;
      case CMD_DUMP:
        nes6502_dump (nes6502_cmd_param, 256);
        break;
      case CMD_IRQ:
        nes6502_irq ();
        break;
      case CMD_NMI:
        nes6502_nmi ();
        break;
      case CMD_RESET:
        nes6502_reset ();
        break;
    }
  } else if (address == IO_PARAMHI_PORT) {
    nes6502_cmd_param &= 0xFF;
    nes6502_cmd_param |= ((uint32) value) << 8;
  } else if (address == IO_PARAMLO_PORT) {
    nes6502_cmd_param &= ~0xFF;
    nes6502_cmd_param |= value;
  }
}

#ifdef DEBUGGER_ENABLED

#include <stdio.h>
#include <string.h>

#ifdef USE_READLINE

#include <readline/readline.h>
#include <readline/history.h>

static char *readline_buffer;

static
char *read_line (const char *prompt)
{
  if (readline_buffer) {
    free (readline_buffer);
    readline_buffer = NULL;
  }
  readline_buffer = readline (prompt);
  if (readline_buffer) {
    if (*readline_buffer) {
      add_history (readline_buffer);
    }
  }

  return readline_buffer;
}
#else

static char readline_buffer[512];
static
char *read_line (const char *prompt)
{
  report ("%s", prompt);
  return fgets (readline_buffer, sizeof (readline_buffer), stdin);
}

#endif

static
int prompt (void)
{
  int pos = 0, argc = 0;
  char *buffer;
  char *args[8];

  buffer = read_line ("> ");

  while (1) {
    while (buffer[pos] == ' ' || buffer[pos] == '\t') pos++;
    if (buffer[pos] == '\r' || buffer[pos] == '\n' || buffer[pos] == '\0') break;

    args[argc++] = &buffer[pos];
    if (argc == (sizeof (args) / sizeof (char *))) break;

    while (buffer[pos] != '\r' && buffer[pos] != '\n' && buffer[pos] != '\0'
           && buffer[pos] != ' ' && buffer[pos] != '\t')
      pos++;

    if (buffer[pos] == ' ' || buffer[pos] == '\t') {
      buffer[pos++] = '\0';
    } else {
      buffer[pos] = '\0';
      break;
    }
  }

  if (argc == 0) return 1;
  if (strcmp ("h", args[0]) == 0) {
    report ("Help\n");
    report (" h                               shows this help\n");
    report (" d [addr]                        dump data at the address\n");
    report (" t [count]                       trace count times\n");
    report (" r                               show registers\n");
    report (" u [addr]                        disassemble at specified address\n");
    report (" bl                              list breakpoints\n");
    report (" bp <addr> <mask> <flags>        set breakpoint\n");
    report (" bc <num> <addr> <mask> <flags>  set breakpoint\n");
    report (" be <num>                        enable breakpoint\n");
    report (" bd <num>                        disable breakpoint\n");
    report (" br <num>                        remove breakpoint\n");
    report (" pw                              power on\n");
    report (" rs                              reset 6502\n");
    report (" irq                             generate a irq\n");
    report (" nmi                             generate a nmi\n");
    report (" q                               quit program\n");
  } else if (strcmp ("d", args[0]) == 0) {
    uint32 address = 0;
    if (argc > 1) sscanf (args[1], "$%x", &address);
    nes6502_dump (address, 256);
  } else if (strcmp ("t", args[0]) == 0) {
    int32 cycles = 0;
    if (argc > 1) sscanf (args[1], "%d", &cycles);
    nes6502_execute (cycles);
    nes6502_printcontext ();
  } else if (strcmp ("r", args[0]) == 0) {
    nes6502_printcontext ();
  } else if (strcmp ("u", args[0]) == 0) {
    uint32 address;
    if (argc > 1) sscanf (args[1], "$%x", &address);
    else {
      nes6502_context cpu;
      nes6502_getcontext (&cpu);
      address = cpu.pc_reg;
    }
    nes6502_unassemble (address, 16);
  } else if (strcmp ("bl", args[0]) == 0) {
    uint32 address, mask, i, count;
    int32 flags;
    count = nes6502_numbreakpoints ();
    for (i = 0; i < count; i++) {
      report ("%d - ", i + 1);
      nes6502_getbreakpoint (i, &address, &mask, &flags);
      report ("$%04X  $%04X - ", address, mask);
      if (flags & NES6502_DEBUG_READ) report ("r");
      if (flags & NES6502_DEBUG_WRITE) report ("w");
      if (flags & NES6502_DEBUG_DISABLED) report ("d");
      report ("\n");
    }
  } else if (strcmp ("be", args[0]) == 0) {
    uint32 address, mask, i;
    int32 flags;
    if (argc == 1) report ("Please specify a breakpoint number\n");
    else {
      sscanf (args[1], "%u", &i);
      i--;
      nes6502_getbreakpoint (i, &address, &mask, &flags);
      flags &= ~NES6502_DEBUG_DISABLED;
      nes6502_editbreakpoint (i, address, mask, flags);
    }
  } else if (strcmp ("bd", args[0]) == 0) {
    uint32 address, mask, i = 0;
    int32 flags;
    if (argc == 1) report ("Please specify a breakpoint number\n");
    else {
      sscanf (args[1], "%u", &i);
      i--;
      nes6502_getbreakpoint (i, &address, &mask, &flags);
      flags |= NES6502_DEBUG_DISABLED;
      nes6502_editbreakpoint (i, address, mask, flags);
    }
  } else if (strcmp ("br", args[0]) == 0) {
    uint32 i = 0;
    if (argc == 1) report ("Please specify a breakpoint number\n");
    else {
      sscanf (args[1], "%u", &i);
      i--;
      nes6502_removebreakpoint (i);
    }
  } else if (strcmp ("bc", args[0]) == 0) {
    uint32 address, mask, i = 0;
    int32 flags;
    if (argc < 5) report ("Please specify the breakpoint number, the address, the mask and the flags\n");
    else {
      sscanf (args[1], "%u", &i);
      i--;
      sscanf (args[2], "$%x", &address);
      sscanf (args[3], "$%x", &mask);
      flags = 0;
      if (strchr (args[4], 'r')) flags |= NES6502_DEBUG_READ;
      if (strchr (args[5], 'w')) flags |= NES6502_DEBUG_WRITE;
      if (strchr (args[6], 'd')) flags |= NES6502_DEBUG_DISABLED;
      nes6502_editbreakpoint (i, address, mask, flags);
    }
  } else if (strcmp ("bp", args[0]) == 0) {
    uint32 address, mask;
    int32 flags;
    if (argc < 4) report ("Please specify the address, the mask and the flags\n");
    else {
      sscanf (args[1], "$%x", &address);
      sscanf (args[2], "$%x", &mask);
      flags = 0;
      if (strchr (args[3], 'r')) flags |= NES6502_DEBUG_READ;
      if (strchr (args[3], 'w')) flags |= NES6502_DEBUG_WRITE;
      if (strchr (args[3], 'd')) flags |= NES6502_DEBUG_DISABLED;
      nes6502_addbreakpoint (address, mask, flags);
    }
  } else if (strcmp ("pw", args[0]) == 0) {
    nes6502_power ();
    nes6502_printcontext ();
  } else if (strcmp ("rs", args[0]) == 0) {
    nes6502_reset ();
    nes6502_printcontext ();
  } else if (strcmp ("irq", args[0]) == 0) {
    nes6502_irq ();
    nes6502_printcontext ();
  } else if (strcmp ("nmi", args[0]) == 0) {
    nes6502_nmi ();
    nes6502_printcontext ();
  } else if (strcmp ("q", args[0]) == 0) {
    return 0;
  } else {
    report ("Unknown command `%s', type `h' for help\n", args[0]);
  }

  return 1;
}

static
void debug_handler (int first_insn)
{
  if (!first_insn)
    nes6502_release ();
}
#endif

int main (int argc, char **argv)
{
  size_t size;
  uint8 *mem;
  nes6502_context cpu;
  nes6502_memread  read_handler;
  nes6502_memwrite write_handler;
  int bank;

  if (argc <= 1) return -1;
  mem = (unsigned char *) read_file (argv[1], &size);
  for (bank = 0; bank < NES6502_NUMBANKS; bank++) {
    cpu.mem_page[bank] = &mem[bank * NES6502_BANKSIZE];
  }

  cpu.read_handler = &read_handler;
  read_handler.min_range = 0x0000;
  read_handler.max_range = 0xFFFF;
  read_handler.read_func = &read_func;

  cpu.write_handler = &write_handler;
  write_handler.min_range = 0x0000;
  write_handler.max_range = 0xFFFF;
  write_handler.write_func = &write_func;

#ifdef DEBUGGER_ENABLED
  cpu.debug_handler = &debug_handler;
#endif

  nes6502_setcontext (&cpu);
  nes6502_power ();

#ifdef USE_READLINE
  rl_bind_key ('\t', rl_abort);
#endif

#ifdef DEBUGGER_ENABLED
  while (prompt ()) {
  }
#else
  while (nes6502_execute (10000)) {
  }
#endif

  nes6502_getcontext (&cpu);
  report ("Total cycles: %u\n", cpu.total_cycles);

  free (mem);
  return 0;
}
