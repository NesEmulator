#ifndef __NES6502_H
#define __NES6502_H

#include "types.h"

#define  NES6502_NUMBANKS  16
#define  NES6502_BANKSHIFT 12
#define  NES6502_BANKSIZE  (0x10000 / NES6502_NUMBANKS)
#define  NES6502_BANKMASK  (NES6502_BANKSIZE - 1)

/* Stack base address (page 1) */
#define NES6502_STACK_OFFSET 0x100

/* Vector addresses */
#define NES6502_NMI_VECTOR     0xFFFA
#define NES6502_RESET_VECTOR   0xFFFC
#define NES6502_IRQ_VECTOR     0xFFFE

/* cycle counts for interrupts */
#define NES6502_INT_CYCLES   7
#define NES6502_RESET_CYCLES 6


/* P (flag) register bitmasks */
#define NES6502_C_FLAG      1
#define NES6502_Z_FLAG      2
#define NES6502_I_FLAG      4
#define NES6502_D_FLAG      8
#define NES6502_B_FLAG     16
#define NES6502_R_FLAG     32
#define NES6502_V_FLAG     64
#define NES6502_N_FLAG    128

typedef struct
{
   uint32 min_range, max_range;
   uint8 (*read_func) (uint32 address);
} nes6502_memread;

typedef struct
{
   uint32 min_range, max_range;
   void (*write_func) (uint32 address, uint8 value);
} nes6502_memwrite;

#ifdef DEBUGGER_ENABLED
#define NES6502_MAX_BREAKPOINTS     8
#define NES6502_DEBUG_READ          1
#define NES6502_DEBUG_WRITE         2
#define NES6502_DEBUG_DISABLED    128
typedef void (*nes6502_debugf) (int first_insn);
#endif

typedef struct
{
   uint8 *mem_page[NES6502_NUMBANKS];  /* memory page pointers */

   nes6502_memread *read_handler;
   nes6502_memwrite *write_handler;

   uint32 pc_reg;
   uint32 a_reg, p_reg;
   uint32 x_reg, y_reg;
   uint32 s_reg;

   int32 total_cycles;
   int32 remaining_cycles;
   int32 burn_cycles;

   int halted;
   int int_pending;

#ifdef DEBUGGER_ENABLED
   nes6502_debugf debug_handler;

   uint32 num_bp;
   uint32 bp_addr[NES6502_MAX_BREAKPOINTS];
   uint32 bp_mask[NES6502_MAX_BREAKPOINTS];
   int32 bp_flags[NES6502_MAX_BREAKPOINTS];

#endif
} nes6502_context;



void nes6502_power (void);
void nes6502_reset (void);
void nes6502_halt (void);
int nes6502_ishalted (void);
int32 nes6502_execute (int32 cycles);

uint8 nes6502_readbyte (uint32 address);
void nes6502_writebyte (uint32 address, uint8 value);
uint8 nes6502_getbyte (uint32 address);
void nes6502_putbyte (uint32 address, uint8 value);

void nes6502_nmi (void);
void nes6502_irq (void);
void nes6502_burn (int32 cycles);
void nes6502_release (void);

void nes6502_setcontext (nes6502_context *cpu);
void nes6502_getcontext (nes6502_context *cpu);

#ifdef DEBUGGER_ENABLED
int nes6502_addbreakpoint (uint32 address, uint32 mask, int32 flags);
void nes6502_editbreakpoint (uint32 bp, uint32 address, uint32 mask, int32 flags);
void nes6502_getbreakpoint (uint32 bp, uint32 *address, uint32 *mask, int32 *flags);
void nes6502_removebreakpoint (uint32 bp);
uint32 nes6502_numbreakpoints (void);
#endif

#endif /* __NES6502_H */
